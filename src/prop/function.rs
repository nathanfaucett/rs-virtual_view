use super::{Prop, Props};

pub type Function = Fn(&mut Props) -> Prop;
